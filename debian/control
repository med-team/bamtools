Source: bamtools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>,
           Andreas Tille <tille@debian.org>,
           Kevin Murray <spam@kdmurray.id.au>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs,
               cmake,
               zlib1g-dev,
               libjsoncpp-dev,
               help2man,
               doxygen,
               pkgconf
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/bamtools
Vcs-Git: https://salsa.debian.org/med-team/bamtools.git
Homepage: https://github.com/pezmaster31/bamtools/wiki
Rules-Requires-Root: no

Package: bamtools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: toolkit for manipulating BAM (genome alignment) files
 BamTools facilitates research analysis and data management using BAM
 files.  It copes with the enormous amount of data produced by current
 sequencing technologies that is typically stored in compressed, binary
 formats that are not easily handled by the text-based parsers commonly
 used in bioinformatics research.
 .
 BamTools provides both a C++ API for BAM file support as well as a
 command-line toolkit.
 .
 This is the bamtools command-line toolkit.
 .
 Available bamtools commands:
  convert  Converts between BAM and a number of other formats
  count    Prints number of alignments in BAM file(s)
  coverage Prints coverage statistics from the input BAM file
  filter   Filters BAM file(s) by user-specified criteria
  header   Prints BAM header information
  index    Generates index for BAM file
  merge    Merge multiple BAM files into single file
  random   Select random alignments from existing BAM file(s), intended more
           as a testing tool.
  resolve  Resolves paired-end reads (marking the IsProperPair flag as needed)
  revert   Removes duplicate marks and restores original base qualities
  sort     Sorts the BAM file according to some criteria
  split    Splits a BAM file on user-specified property, creating a new BAM
           output file for each value found
  stats    Prints some basic statistics from input BAM file(s)

Package: libbamtools-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libbamtools2.5.2 (= ${binary:Version}),
         ${misc:Depends},
         ${devlibs:Depends}
Suggests: libbamtools-doc (= ${binary:Version})
Breaks: libbamtools2.5.1
Replaces: libbamtools2.5.1
Description: C++ API for manipulating BAM (genome alignment) files
 BamTools facilitates research analysis and data management using BAM
 files.  It copes with the enormous amount of data produced by current
 sequencing technologies that is typically stored in compressed, binary
 formats that are not easily handled by the text-based parsers commonly
 used in bioinformatics research.
 .
 BamTools provides both a C++ API for BAM file support as well as a
 command-line toolkit.
 .
 This is the developers API package.

Package: libbamtools2.5.2
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: dynamic library for manipulating BAM (genome alignment) files
 BamTools facilitates research analysis and data management using BAM
 files.  It copes with the enormous amount of data produced by current
 sequencing technologies that is typically stored in compressed, binary
 formats that are not easily handled by the text-based parsers commonly
 used in bioinformatics research.
 .
 BamTools provides both a C++ API for BAM file support as well as a
 command-line toolkit.
 .
 This is the runtime library.

Package: libbamtools-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         libjs-jquery
Description: docs for dynamic library for manipulating BAM (genome alignment) files
 BamTools facilitates research analysis and data management using BAM
 files.  It copes with the enormous amount of data produced by current
 sequencing technologies that is typically stored in compressed, binary
 formats that are not easily handled by the text-based parsers commonly
 used in bioinformatics research.
 .
 BamTools provides both a C++ API for BAM file support as well as a
 command-line toolkit.
 .
 This is the documentation for the library.
